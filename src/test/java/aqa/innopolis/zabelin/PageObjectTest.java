package aqa.innopolis.zabelin;

import aqa.innopolis.zabelin.pages.MainPage;
import aqa.innopolis.zabelin.pages.TicketPage;
import org.junit.jupiter.api.Assertions;

public abstract class PageObjectTest {

    public static void runPageObjectTest() throws NullPointerException {

        String title = "ZABELIN";
        String email = "zabelin@innopolis.aqa";

        TicketPage ticketPage = new MainPage()
                .createTicket(title, "Test of PO", email)
                .openLoginPage()
                .auth("demo", "demo1234")
                .findTicket(title);

        Assertions.assertEquals(ticketPage.getEmail(), email);
    }
}
