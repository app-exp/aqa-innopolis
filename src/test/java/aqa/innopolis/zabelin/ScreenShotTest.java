package aqa.innopolis.zabelin;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.codeborne.selenide.Selenide.$;

public abstract class ScreenShotTest extends TestHelper {

    public static void runScreenShotTest() throws IOException {

        Configuration.reportsFolder = Settings.getScreenShotPath();
        Files.deleteIfExists(Paths.get(Settings.getScreenShotPath() + "actualScreen.png"));
        Files.deleteIfExists(Paths.get(Settings.getScreenShotPath() + "resultScreen.png"));

        String textRequest = "Innopolis";
        Selenide.open(Settings.getUrlForScreen());//open("https://www.wikipedia.org");
        $(By.id("searchInput")).sendKeys(textRequest);
        $(By.xpath("//button[@type='submit']")).click();
        $(By.cssSelector("#firstHeading")).click();//h1[@id='firstHeading']

        Selenide.screenshot("actualScreen");

        File actualFile = new File(Settings.getScreenShotPath() + "actualScreen.png");
        File expectedFile = new File(Settings.getScreenShotPath() + "expectedScreen.png");

        BufferedImage actualImage = ImageIO.read(actualFile);
        BufferedImage expectedImage = ImageIO.read(expectedFile);

        Screenshot actualScreenshot = new Screenshot(actualImage);
        Screenshot expectedScreenshot = new Screenshot(expectedImage);

        ImageDiffer imageDiffer = new ImageDiffer();
        ImageDiff imageDiff = imageDiffer.makeDiff(expectedScreenshot, actualScreenshot);

        if (imageDiff.getDiffSize() > 0) {
            File result = new File(Settings.getScreenShotPath() + "resultScreen.png");
            ImageIO.write(imageDiff.getMarkedImage(), "png", result);
        }

        Assertions.assertEquals(imageDiff.getDiffSize(), 0);
    }
}
