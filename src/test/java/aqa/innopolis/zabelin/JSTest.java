package aqa.innopolis.zabelin;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;

import java.util.Locale;

import static com.codeborne.selenide.Selenide.*;

public abstract class JSTest extends TestHelper {

    public static void runJSTest() {

        Selenide.open(Settings.getUrlForJS());
        SelenideElement element = $x("//*[@for='id_queue']");
        String elementText = element.getText();
        String newText = "INNOPOLIS";
        Selenide.executeJavaScript(String.format("arguments[0].innerText='%s'", newText), element);

        Selenide.executeJavaScript("document.querySelector(\"#id_queue > option:nth-child(1)\").selected=true");
        Selenide.executeJavaScript("document.querySelector(\"#id_queue > option:nth-child(2)\").selected=true");
        Selenide.executeJavaScript("document.querySelector(\"#id_queue > option:nth-child(3)\").selected=true");

        Selenide.executeJavaScript(String.format("document.querySelector(\"#id_title\").value='%s'", "ZABELIN"));
        Selenide.executeJavaScript(String.format("document.querySelector(\"#id_body\").value='%s'", "Test of JS"));
        Selenide.executeJavaScript(String.format("document.querySelector(\"#id_submitter_email\").value='%s'", "ZABELIN@INNOPOLIS.AQA".toLowerCase(Locale.ROOT)));

        $(By.xpath("//button[@class='btn btn-primary btn-lg btn-block']")).click();

        Assertions.assertNotEquals(elementText, newText);
    }
}