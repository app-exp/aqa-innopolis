package aqa.innopolis.zabelin;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

public abstract class Settings {

    private static final Properties prop = new Properties();

    static {
        try {
            prop.load(TestHelper.class.getClassLoader().getResourceAsStream("settings.properties"));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private static String loadProperty(String key) {
        return prop.getProperty(key).trim();
    }

    public static String getUrlForJson() {
        return loadProperty("urlForJson");
    }

    public static String getUrlForScreen() {
        return loadProperty("urlForScreen");
    }

    public static String getUrlForPO() {
        return loadProperty("urlForPO");
    }

    public static String getUrlForJS() {
        return loadProperty("urlForJS");
    }

    public static String getUrlForResize() {
        return loadProperty("urlForResize");
    }

    public static String getScreenShotPath() {
        return System.getProperty("user.dir")
                + File.separator + "src"
                + File.separator + "test"
                + File.separator + "screens"
                + File.separator;
    }

    public static String getTestDir() {

        return System.getProperty("user.dir")
                + File.separator + "src"
                + File.separator + "test"
                + File.separator;
    }
}