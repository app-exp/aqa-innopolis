package aqa.innopolis.zabelin;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public abstract class JsonTest extends TestHelper {

    public static void runJsonTest() throws IOException, ParserConfigurationException, SAXException, JSONException {

        String fieldName = "error";

        String urlForJson = Settings.getUrlForJson();

        File file = new File(Settings.getTestDir() + "resources" + File.separator + "jsonLoginPass.xml");
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(file);

        NodeList logins = document.getElementsByTagName("login");

        for (int i = 0; i < logins.getLength(); i++) {
            Node item = logins.item(i);
            String login = item.getTextContent().trim();
            String password = item.getAttributes().getNamedItem("password").getNodeValue().trim();

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("email", login);
            if (!password.equals("")) {
                jsonObject.put("password", password);
            }

            Response response = HttpRequest.httpRequestPOST(urlForJson, jsonObject);

            JsonPath jsonPath = response.jsonPath();
            String fieldValue = jsonPath.get(fieldName);

            JSONObject jsonObjectResponse = new JSONObject();
            jsonObjectResponse.put(fieldName, fieldValue);

            boolean status = false;
            try {
                jsonObjectResponse.get(fieldName);
                status = true;
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            Assertions.assertTrue(status);
        }
    }
}
