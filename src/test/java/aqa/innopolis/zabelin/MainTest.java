package aqa.innopolis.zabelin;

import com.codeborne.selenide.Configuration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class MainTest {

    @AfterEach
    public void setDefaultSize() {
        Configuration.browserSize = "1280x720";
    }

    @Test
    void jsonTest() throws IOException, ParserConfigurationException, SAXException {
        JsonTest.runJsonTest();
    }

    @Test
    void screenShotTest() throws IOException {
        ScreenShotTest.runScreenShotTest();
    }

    @Test
    void jsTest() {
        JSTest.runJSTest();
    }

    @Test
    void resizeTest() {
        ResizeTest.runResizeTest();
    }
}