package aqa.innopolis.zabelin;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.junit.jupiter.api.Assertions;

import static com.codeborne.selenide.Selenide.$x;

public abstract class ResizeTest extends TestHelper {

    private static final String urlForResize = Settings.getUrlForResize();
    private static final String orderButtonXPath = "//span[@data-test-id='button__my-orders']";

    public static void runResizeTest() {

        try {
            Selenide.closeWindow();
            Selenide.closeWebDriver();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        boolean small = isButtonDisplayed("400x300");
        boolean big = isButtonDisplayed("1200x300");

        Assertions.assertNotEquals(small, big);
    }

    private static boolean isButtonDisplayed(String browserSize) {

        Configuration.browserSize = browserSize;
        Selenide.open(urlForResize);
        SelenideElement orderButton = $x(orderButtonXPath);
        boolean isDisplayed = orderButton.isDisplayed();
        Selenide.closeWindow();
        Selenide.closeWebDriver();
        return isDisplayed;
    }
}
