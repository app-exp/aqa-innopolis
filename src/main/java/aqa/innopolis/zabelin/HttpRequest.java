package aqa.innopolis.zabelin;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

import org.json.JSONObject;

public abstract class HttpRequest {

    public static Response httpRequestPOST(String urlRequest, JSONObject jsonObject) {

        Response response = given()
                .contentType(ContentType.JSON)
                .and()
                .body(jsonObject)
                .when()
                .post(urlRequest)
                .then()
                .extract()
                .response();

        return response;
    }
}
