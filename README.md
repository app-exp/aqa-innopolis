<h1>Проект содержит реализацию итоговой аттестации по курсу</h1>
<h1>STC-22-02 Автоматизированное тестирование ПО на Java</h1>
<h1>Университета: https://innopolis.university</h1>

<h2>ТЕМА: Автоматизированное тестирование UI</h2>
<h2>ЦЕЛЬ: Автоматизация тестирования UI для демонстрации навыков в области автотестирования</h2>

<h3>ОКРУЖЕНИЕ:</h3>
Операционная система Windows 10 Pro версия 21H2 сборка ОС 19044.1645 x64
<br>Среда разработки IntelliJ IDEA Educational Edition 2021.3
<br>Corretto 8 JDK 8.312.07.1
<br>Web-браузер Google Chrome 101.0.4951.54 (Официальная сборка)(64 бит)
<br>Драйвер для автоматического запуска: https://chromedriver.chromium.org/downloads

<h3>ИСПОЛЬЗОВАННЫЙ ИНСТРУМЕНТАРИЙ:</h3>
Java
<br>JavaScript
<br>REST Assured
<br>Selenium
<br>Selenide
<br>JUnit
<br>Allure

<h3>САЙТЫ, ИСПОЛЬЗОВАННЫЕ ДЛЯ ТЕСТИРОВАНИЯ:</h3>
https://www.wikipedia.org
<br>https://django-helpdesk-demo.herokuapp.com
<br>https://reqres.in/api/login
<br>https://kazanexpress.ru
